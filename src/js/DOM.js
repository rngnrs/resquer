let DOM = {
  add: function (el, parent) {
    parent = parent || document.body;
    parent.appendChild(el);
  },
  remove: function (el, parent) {
    parent = parent || document.body;
    parent.removeChild(el);
  },
  create: function (tag, cl, text, id) {
    let el = document.createElement(tag);
    if (cl !== null) {
      if (!Array.isArray(cl)) cl = [ cl ];
      cl.forEach(function(c) {
        el.classList.add(c || '');
      });
    }
    if (id) el.id = id;
    if (text) el.innerText = text;
    return el;
  },
  getById: function(id, parent) {
    parent = parent || document;
    return parent.getElementById(id);
  },
  block: function(cl, parent) {
    parent = parent || document;
    return parent.querySelector('.'+cl);
  },
  blocks: function(cl, parent) {
    parent = parent || document;
    return parent.querySelectorAll('.'+cl);
  }
};
