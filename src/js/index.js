class RSQ {
  constructor (arr) {
    if (!Array.isArray(arr)) {
      arr = [ arr ];
    }
    this.queue = arr;
    this.fileCount = 0;
    this.loadedCount = [];
  }

  request (src, next) {
    let url = Array.isArray(src)
      ? src.shift()
      : src;

    this.fileCount++;

    let popup;
    if (typeof Noti !== 'undefined') {
      popup = new Noti.popup(url, 'info');
    }

    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = () => {
      if (xhr.status >= 400) {
        console.error(url + ' - ' + xhr.statusText);
      } else {
        let head = document.getElementsByTagName('head')[0];

        if (/.js$/.test(url)) {
          let node = document.createElement('script');
          node.innerHTML = xhr.response;
          head.appendChild(node);
        }

        if (/.css$/.test(url)) {
          let css = document.createElement('style');
          css.type = 'text/css';
          if (css.styleSheet) {
            css.styleSheet.cssText = xhr.response;
          } else {
            css.appendChild(document.createTextNode(xhr.response));
          }
          head.appendChild(css);
        }
      }
      this.loadedCount.push(url);
      if (Array.isArray(src) && src.length) {
        return this.request(src, next);
      }
      return next(42, url);
    };

    xhr.onerror = (event) => {
      console.log('error: ', url, xhr, event);
    };

    let onProgressHandler = (event) => {
      if(event.lengthComputable) {
        let progress = (event.loaded / event.total);
        //console.log((this.loadedCount.length + progress) + '/' + this.fileCount);
        if (popup) {
          popup.edit(url + ': ' + Math.floor(progress * 100));
        }
      }
    };
    xhr.addEventListener('progress', onProgressHandler);
    xhr.send();
  }

  run () {
    for (let i = 0; i < this.queue.length; i++) {
      this.request(this.queue[i], console.log);
    }
  }
}

let css = ['css/index.min.css'];
let js = ['js/DOM.min.js', 'js/popup.min.js', 'js/test.min.js'];
let Loader = new RSQ([css, js]);
Loader.run();
