DOM.add(DOM.create('div', 'popups'));
let Noti = {};
Noti.popups = [];
Noti.popup = function (text, type, timeout) {
  if (!Array.isArray(text))
    text = [ null, text ];
  this.hideTimer = null;
  this.timeout = timeout || 5000;
  this.text = DOM.create('div', ['popups__popup', 'popups__popup_' + (type || 'info')]);
  let textEl = DOM.create('div', 'popups__text', '');
  if (text[0])
    DOM.add(DOM.create('div','popups__bold', text[0]), textEl);
  DOM.add(DOM.create('pre', null, text[1]), textEl);
  DOM.add(textEl, this.text);
  this.show();
  this.text.ondblclick = () => this.hide();
  this.text.onmousemove = () => this.resetTimeout();
};
Noti.popup.prototype.show = function () {
  if (this.hideTimer) return;
  Noti.popups.push(this);
  DOM.block('popups').appendChild(this.text);
  setTimeout(() => this.text.classList.add('popups__popup_visible'), 10);
  this.hideTimer = setTimeout(this.hide.bind(this), this.timeout);
};
Noti.popup.prototype.edit = function (text) {
  if (!Array.isArray(text))
    text = [ null, text ];

  let string = DOM.block('popups__text', this.text);
  if (!string) {
    return false;
  }
  if (text[0]) {
    let boldString = DOM.block('popups__bold', string);
    if (boldString) {
      boldString.innerHTML = text[0];
    } else {
      DOM.add(DOM.create('div','popups__bold', text[0]), string);
    }
  }
  string.getElementsByTagName('pre')[0].innerHTML = text[1];
  this.hideTimer = setTimeout(this.hide.bind(this), this.timeout);
};
Noti.popup.prototype.hide = function () {
  if (!this.hideTimer) return;
  clearTimeout(this.hideTimer);
  this.hideTimer = null;
  this.text.classList.remove('popups__popup_visible');
  setTimeout(() => DOM.block('popups').removeChild(this.text), 500);
  let i = Noti.popups.indexOf(this);
  Noti.popups.splice(i, 1);
};
Noti.popup.prototype.resetTimeout = function (timeout) {
  if (!this.hideTimer) return;
  clearTimeout(this.hideTimer);
  this.timeout = timeout || 5000;
  this.hideTimer = setTimeout(this.hide.bind(this), this.timeout);
};
