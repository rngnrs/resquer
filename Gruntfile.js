module.exports = function(grunt) {
  grunt.initConfig({
    babel: {
      options: {
        sourceMap: false
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'src/js',
          src: '**/*.js',
          dest: 'public/js',
          ext: '.min.js'
        }]
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'src/css',
          src: ['**/*.css', '!**/*.min.css'],
          dest: 'public/css',
          ext: '.min.css'
        }]
      }
    },
		watch: {
			styles: {
		    files: ['src/css/**/*.css'],
		    tasks: ['cssmin'],
		    options: {
		      spawn: false,
		    },
		  },
			scripts: {
		    files: ['src/js/**/*.js'],
		    tasks: ['babel'],
		    options: {
		      spawn: false,
		    },
		  }
		}
  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-babel');

	grunt.registerTask('default', ['babel', 'cssmin', 'watch']);
};
